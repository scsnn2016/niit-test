#define _CRT_SECURE_NO_WARNINGS
#include <stdio.h>
#include <math.h>
#include <windows.h>
int main()
{
	printf("Enter land size:\n");
	float a, b, c, tax, p, s, price;
	printf("1 side: ");
	scanf("%f", &a);
	printf("2 side: ");
	scanf("%f", &b);
	printf("3 side: ");
	scanf("%f", &c);
	while (a + b <= c || a + c <= b || b + c <= a)
	{
		printf("ERROR! Pliase? repite!\n");
		printf("Enter land size:\n");
		printf("1 side: ");
		scanf("%f", &a);
		printf("2 side: ");
		scanf("%f", &b);
		printf("3 side: ");
		scanf("%f", &c);
	}
	printf("Enter price in rubles for 1 square meter: ");
	scanf("%f", &tax);
	p = (a + b + c) / 2;
	s = sqrt(p*(p - a)*(p - b)*(p-c));
	price = tax*s;
	printf("Price for land is %25.2f rubles", price);
	return 0;
}